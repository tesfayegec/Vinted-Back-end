/*
-- Query: SELECT * FROM vinted.categories
LIMIT 0, 1000

-- Date: 2022-01-28 14:52
*/
INSERT INTO `categories` (`id`,`name`) VALUES (1,'shoes');
INSERT INTO `categories` (`id`,`name`) VALUES (2,'technology');
INSERT INTO `categories` (`id`,`name`) VALUES (3,'clothes');
INSERT INTO `categories` (`id`,`name`) VALUES (4,'books');
INSERT INTO `categories` (`id`,`name`) VALUES (5,'cook');
INSERT INTO `categories` (`id`,`name`) VALUES (6,'accessories');
