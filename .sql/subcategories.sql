/*
-- Query: SELECT * FROM vinted.subcategories
LIMIT 0, 1000

-- Date: 2022-01-28 14:53
*/
INSERT INTO `subcategories` (`id`,`name`,`category_id`,`categoryId`) VALUES (1,'shoes',1,NULL);
INSERT INTO `subcategories` (`id`,`name`,`category_id`,`categoryId`) VALUES (2,'book',2,NULL);
INSERT INTO `subcategories` (`id`,`name`,`category_id`,`categoryId`) VALUES (3,'technology',3,NULL);
INSERT INTO `subcategories` (`id`,`name`,`category_id`,`categoryId`) VALUES (4,'clothes',4,NULL);
INSERT INTO `subcategories` (`id`,`name`,`category_id`,`categoryId`) VALUES (5,'cook',5,NULL);
INSERT INTO `subcategories` (`id`,`name`,`category_id`,`categoryId`) VALUES (6,'accesories',6,NULL);
